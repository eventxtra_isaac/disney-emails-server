var pug = require('pug');
var nodemailer = require('nodemailer');
var mandrillTransport = require('nodemailer-mandrill-transport');

var transport = nodemailer.createTransport(mandrillTransport({
  auth: {
    apiKey: '4ZvttbQrZEBptEY8J9kj1A'
  }
}))

var html = pug.renderFile('templates/email.pug')

var mailOptions = {
  from: 'Me <isaac@eventxtra.com>', // sender address
  to: ['isaac@eventxtra.com'], // list of receivers
  subject: 'Disney Email', // Subject line
  html: html,// plain text body
}

transport.sendMail(mailOptions, function(err, msg){
  if(err){
      console.log(err);
  }
  else {
      console.log(msg);
  }
})